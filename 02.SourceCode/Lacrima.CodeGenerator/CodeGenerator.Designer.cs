﻿namespace Lacrima.CodeGenerator
{
    partial class CodeGenerator
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_GenerateCode = new System.Windows.Forms.Button();
            this.Btn_Save = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Btn_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Btn_GenerateCode
            // 
            this.Btn_GenerateCode.Location = new System.Drawing.Point(789, 558);
            this.Btn_GenerateCode.Name = "Btn_GenerateCode";
            this.Btn_GenerateCode.Size = new System.Drawing.Size(107, 29);
            this.Btn_GenerateCode.TabIndex = 0;
            this.Btn_GenerateCode.Text = "生成(&G)";
            this.Btn_GenerateCode.UseVisualStyleBackColor = true;
            this.Btn_GenerateCode.Click += new System.EventHandler(this.Btn_GenerateCode_Click);
            // 
            // Btn_Save
            // 
            this.Btn_Save.Location = new System.Drawing.Point(936, 558);
            this.Btn_Save.Name = "Btn_Save";
            this.Btn_Save.Size = new System.Drawing.Size(107, 29);
            this.Btn_Save.TabIndex = 1;
            this.Btn_Save.Text = "保存(&S)";
            this.Btn_Save.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 157);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1178, 386);
            this.textBox1.TabIndex = 2;
            // 
            // Btn_Close
            // 
            this.Btn_Close.Location = new System.Drawing.Point(1083, 558);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(107, 29);
            this.Btn_Close.TabIndex = 3;
            this.Btn_Close.Text = "关闭(&C)";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // CodeGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 599);
            this.Controls.Add(this.Btn_Close);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Btn_Save);
            this.Controls.Add(this.Btn_GenerateCode);
            this.Name = "CodeGenerator";
            this.Text = "CodeGenerator";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CodeGenerator_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_GenerateCode;
        private System.Windows.Forms.Button Btn_Save;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Btn_Close;
    }
}

